---
title: Річний звіт про діяльність закладу освіти
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/16.jpg
show_sidebar: true
menubar: example_menu
---
<div align="center"><b>А Н А Л І З<br>
освітньої діяльності школи<br>
за 2018-2019 навчальний рік</b></div>

1. Інформаційна карта

Середня загальноосвітня школа І-ІІІ ступенів № 17 розташована за адресою: Донецька область, м Краматорськ, вулиця Благодатна № 80, тел. 41-76-67,41-76-50.

2. Загальні показники

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td>1</td>
<td>Мова навчання</td>
<td>Українська з 2000 года, російська</td>
</tr>
<tr>
<td>2</td>
<td>Кількість класів<br><br>
- російських<br>
- українських<br>
- профільних<br>
</td>
<td>21<br><br /> 8<br /> 13<br /> 3</td>
</tr>
<tr>
<td>3</td>
<td>Кількість ГПД</td>
<td>1</td>
</tr>
<tr>
<td>4</td>
<td>Загальна кількість вихованців</td>
<td>518</td>
</tr>
<tr>
<td>5</td>
<td>Учнів, що навчаються у першу зміну<br />
<strong>другу зміну</strong>
</td>
<td>518<br /><strong>40</strong></td>
</tr>
<tr>
<td>6</td>
<td>Охоплення вихованців гарячим харчуванням</td>
<td>483</td>
</tr>
<tr>
<td>7</td>
<td>Кількість працівників всього</td>
<td>63</td>
</tr>
<tr>
<td>8</td>
<td>В т.ч: педагогічних</td>
<td style="text-align: left;">44</td>
</tr>
<tr>
<td>9</td>
<td>обслуговуючих</td>
<td>19</td>
</tr>
<tr>
<td>10</td>
<td>Кількість робочих місць у майстернях</td>
<td>20/20/20/20</td>
</tr>
<tr>
<td>11</td>
<td>Кількість робочих місць в комп'ютерному класі</td>
<td>15</td>
</tr>
<tr>
<td>12</td>
<td>Забезпеченість підручниками та навчальними текстами</td>
<td>97%</td>
</tr>
<tr>
<td>13</td>
<td>Число книг у шкільній бібліотеці / на одного учня /</td>
<td>12</td>
</tr>
<tr>
<td>14</td>
<td>Кількість їдалень</td>
<td>1</td>
</tr>
<tr>
<td>15</td>
<td>Кількість посадочних місць в їдальні</td>
<td>150</td>
</tr>
<tr>
<td>16</td>
<td>Площа земельної ділянки</td>
<td>4524,3м<sup>2</sup></td>
</tr>
</tbody>
</table>

1. Виконання основних завдань 2019-2020 навчального року

 

УПРАВЛІНСЬКИЙ МОНІТОРИНГ

 
Аналіз роботи школи щодо забезпечення прав молоді на здобуття освіти проведений на основі бази даних «Мікрорайон школи». Всі діти і підлітки шкільного віку, що проживають в мікрорайоні школи, здобувають загальну середню освіту. Набір дітей в перші класи здійснювався на підставі висновків медичної комісії, за результатами вивчення психологічної готовності дитини до шкільного навчання і бажання батьків. При оформленні мережі перших класів проводилося опитування батьків про вибір мови навчання.    
У вивченні доцільності використання та діагностики стану реалізації варіантної складової навчального плану брали участь 283 учнів і 121 батьків. На підставі результативного рейтингу предметів був складений навчальний план на 2019-2020 навчальний рік / навчальний план додається /.   
Навчально-виховний процес був організований згідно з навчальним планом роботи школи. Планування річної роботи передувала система заходів, яка об'єднує різноманітні форми і проводиться на різних рівнях.   
Реалізація інваріантної і варіантної складової навчального плану здійснювалася за державними, регіональними програмами. Шкільний комплекс повністю забезпечений програмами навчально-методичних комплексів.
   
 
Мониторинг якості освіти

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td style="text-align: center;" rowspan="2">№ з/п</td>
<td style="text-align: center;" rowspan="2">Навчальні предмети</td>
<td style="text-align: center;" colspan="4">Стартова діагностика</td>
<td style="text-align: center;" colspan="4">Підсумкова діагностика</td>
</tr>
<tr>
<td style="text-align: left;">вис.</td>
<td style="text-align: left;">дост.</td>
<td style="text-align: left;">сер.</td>
<td style="text-align: left;">низ.</td>
<td style="text-align: left;">вис.</td>
<td style="text-align: left;">дост.</td>
<td style="text-align: left;">сер.</td>
<td style="text-align: left;">низ.</td>
</tr>
<tr>
<td>1</td>
<td>Російська мова</td>
<td>8,0</td>
<td>45,8</td>
<td>32,7</td>
<td>13,5</td>
<td>10,5</td>
<td>42,9</td>
<td>36</td>
<td>15</td>
</tr>
<tr>
<td>2</td>
<td>Українська мова</td>
<td>6,9</td>
<td>45,6</td>
<td>43,3</td>
<td>4,8</td>
<td>11,1</td>
<td>51,1</td>
<td>33,3</td>
<td>5,3</td>
</tr>
<tr>
<td>3</td>
<td>Англійська мова</td>
<td>15,3</td>
<td>51,6</td>
<td>26,9</td>
<td>6,8</td>
<td>25,3</td>
<td>44,1</td>
<td>28,5</td>
<td>2,9</td>
</tr>
<tr>
<td>4</td>
<td>Історія</td>
<td>18,9</td>
<td>48,5</td>
<td>28,5</td>
<td>4,9</td>
<td>26,6</td>
<td>51,7</td>
<td>23</td>
<td>1,3</td>
</tr>
<tr>
<td>5</td>
<td>Правознавство</td>
<td>35,6</td>
<td>38,7</td>
<td>19,4</td>
<td>9</td>
<td>40</td>
<td>38,3</td>
<td>21,1</td>
<td>5</td>
</tr>
<tr>
<td>6</td>
<td>Математика</td>
<td>11,4</td>
<td>25,9</td>
<td>42,5</td>
<td>20,9</td>
<td>22,4</td>
<td>38,1</td>
<td>31</td>
<td>9,1</td>
</tr>
<tr>
<td>7</td>
<td>Географія</td>
<td>7</td>
<td>41,8</td>
<td>45,9</td>
<td>7,5</td>
<td>10,6</td>
<td>57,7</td>
<td>31,3</td>
<td>1,3</td>
</tr>
<tr>
<td>8</td>
<td>Біологія</td>
<td>8</td>
<td>37,5</td>
<td>40,9</td>
<td>15,9</td>
<td>11,1</td>
<td>42,8</td>
<td>37,5</td>
<td>9,7</td>
</tr>
<tr>
<td>9</td>
<td>Хімія</td>
<td>8,9</td>
<td>22,7</td>
<td>45,6</td>
<td>23,5</td>
<td>17,1</td>
<td>43,6</td>
<td>33</td>
<td>8,9</td>
</tr>
<tr>
<td>10</td>
<td>Фізика</td>
<td>18,4</td>
<td>20,9</td>
<td>46,6</td>
<td>14,9</td>
<td>15,7</td>
<td>35,6</td>
<td>41,5</td>
<td>7,6</td>
</tr>
</tbody>
</table>


Участь в олімпіадах, конкурсах, турнірах

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td>1 місце в міській олімпіаді</td>
<td style="text-align: center;">2</td>
</tr>
<tr>
<td>1 місце в обласній олімпіаді</td>
<td style="text-align: center;">1</td>
</tr>
<tr>
<td>2 місце в міській олімпіаді</td>
<td style="text-align: center;">3</td>
</tr>
<tr>
<td>3 місце в міській олімпіаді</td>
<td style="text-align: center;">4</td>
</tr>
<tr>
<td>Конкурси, турніри - 102</td>
<td style="text-align: center;">(1-3 місця)</td>
</tr>
</tbody>
</table>


Участь школярів у роботі гуртків та секцій

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td>5-і класи</td>
<td style="text-align: center;">28 учнів</td>
</tr>
<tr>
<td>6-і класи</td>
<td style="text-align: center;">35 учнів</td>
</tr>
<tr>
<td>7-і класи</td>
<td style="text-align: center;">34 учнів</td>
</tr>
<tr>
<td>8-і класи</td>
<td style="text-align: center;">29 учнів</td>
</tr>
<tr>
<td>9-і класи</td>
<td style="text-align: center;">19 учнів&nbsp;</td>
</tr>
<tr>
<td>10-і класи</td>
<td style="text-align: center;">18 учнів&nbsp;</td>
</tr>
<tr>
<td>11-і класи</td>
<td style="text-align: center;">14 учнів</td>
</tr>
</tbody>
</table>


<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td style="vertical-align: top;" rowspan="5">З них: </td>
<td>спортивні гуртки</td>
<td>- 91 учень</td>
</tr>
<tr>
<td>музичні</td>
<td>- 53 учні</td>
</tr>
<tr>
<td>танцювальні</td>
<td>- 36 учнів</td>
</tr>
<tr>
<td>прикладні</td>
<td>- 27 учнів</td>
</tr>
<tr>
<td>театральні</td>
<td>- 8 учнів</td>
</tr>
</tbody>
</table>


<div align="center"><b>Підсумки соціально-економічного розвитку школи</b></div>

Протягом року в навчальному закладі були проведені заходи щодо економії бюджетних коштів: встановлено жорсткий контроль за використання енерго-, тепло-, і водоресурсів. Поряд із заходами щодо економії бюджетних коштів здійснювалися пошуки шляхів залучення позабюджетного фінансування.
У школі продовжено процес створення необхідних умов для вдосконалення виховного процесу. Удосконалено кабінет інформатики, спортивна зала, повністю забезпечені обладнанням та меблями 2 перших класи, в рамках соціального проекту НКМЗ капітально відремонтована актова зала та замінена на леноліум підлога в бібліотеці, медичному кабінеті, в кабінеті заступника директора з виховної роботи, відкрито сучасний кабінет математики, продовжуються роботи по термомодернізації школи, капітально відремонтована стеля 3 поверху шкільної будівлі.
У новому навчальному році необхідно здійснити концентрацію коштів і матеріальних ресурсів на виконання невідкладних робіт з ремонту асфальтного покриття шкільного двору і пожежного обладнання, оновити оформлення шкільних коридорів, вирішити питання із капітальним ремонтом шкільної їдальні, виконати капітальний ремонт майстерні школи.
Підводячи підсумки роботи педколективу за 2018-2019 навчальний рік, необхідно відзначити, що не всі проблеми навчально-виховної роботи вирішуються легко і мають позитивний результат:
- Серйозної уваги вимагає виховання в учнів інтересу до знань, почуття патріотизму, моральних принципів, етики спілкування між ними, відповідального ставлення до здорового способу життя.
- Серйозною залишається проблема відсторонення батьків від проблем дітей і школи, їх безвідповідальності перед дітьми і Законом.
- На низькому рівні залишається учнівське самоврядування через відсутність лідерських якостей у дітей.
- Необхідна координація зусиль педагогічного колективу по формуванню у школярів соціального досвіду, готовності жити в демократичному суспільстві, суворо виконуючи закони держави.