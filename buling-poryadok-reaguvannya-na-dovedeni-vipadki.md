---
title: Порядок реагування на доведені випадки булінгу у школі
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/2.jpg
show_sidebar: true
menubar: example_menu
---
<div align="center"><b>Реагування на доведені випадки булінгу</b></div>

1. На основі рішення комісії з розгляду випадків булінгу (цькування), яка кваліфікувала випадок як булінг (цькування), а не одноразовий конфлікт чи сварка, тобто відповідні дії носять систематичний характер, директор школи:
- повідомляє уповноваженим підрозділам органів Національної поліції України (ювенальна поліція) та службі у справах дітей про випадки булінгу (цькування) в закладі освіти;
- забезпечує виконання заходів для надання соціальних та психолого-педагогічних послуг здобувачам освіти, які вчинили булінг, стали його свідками або постраждали від булінгу (цькування) (далі – Заходи).
2. Заходи здійснюються заступником директора з виховної роботи у взаємодії з практичним психологом школи  та затверджуються директором закладу.
3. З метою виконання Заходів можна запроваджувати консультаційні години у практичного психолога, створювати скриньки довіри, оприлюднювати телефони довіри.

<div align="center"><b>Відповідальність осіб причетних до булінгу (цькування)</b></div>

1. Відповідальність за булінг (цькування) встановлена статтею 173 п.4 Кодексу України про адміністративні правопорушення такого змісту:   
«Стаття 173 п.4» . Булінг (цькування) учасника освітнього процесу.   
Булінг (цькування), тобто діяння учасників освітнього процесу, які полягають у психологічному, фізичному, економічному, сексуальному насильстві, у тому числі із застосуванням засобів електронних комунікацій, що вчиняються стосовно малолітньої чи неповнолітньої особи або такою особою стосовно інших учасників освітнього процесу, внаслідок чого могла бути чи була заподіяна шкода психічному або фізичному здоров’ю потерпілого, - тягне за собою накладення штрафу від п’ятдесяти до ста неоподатковуваних мінімумів доходів громадян або громадські роботи на строк від двадцяти до сорока годин.
Діяння, передбачене частиною першою цієї статті, вчинене групою осіб або повторно протягом року після накладення адміністративного стягнення, - тягне за собою накладення штрафу від ста до двохсот неоподатковуваних мінімумів доходів громадян або громадські роботи на строк від сорока до шістдесяти годин.
Діяння, передбачене частиною першою цієї статті, вчинене малолітніми або неповнолітніми особами віком від чотирнадцяти до шістнадцяти років, -тягне за собою накладення штрафу на батьків або осіб, які їх замінюють, від п’ятдесяти до ста неоподатковуваних мінімумів доходів громадян або громадські роботи на строк від двадцяти до сорока годин.
Діяння, передбачене частиною другою цієї статті, вчинене малолітньою або неповнолітньою особою віком від чотирнадцяти до шістнадцяти років, - тягне за собою накладення штрафу на батьків або осіб, які їх замінюють, від ста до двохсот неоподатковуваних мінімумів доходів громадян або громадські роботи на строк від сорока до шістдесяти годин.
Неповідомлення директором закладу уповноваженим підрозділам органів Національної поліції України про випадки булінгу (цькування) учасника освітнього процесу – тягне за собою накладення штрафу від п’ятдесяти до ста неоподатковуваних мінімумів доходів громадян або виправні роботи на строк до одного місяця з відрахуванням до двадцяти процентів заробітку.