---
title: ЗНО
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/15.jpg
show_sidebar: true
menubar: example_menu
---

<div id="lightgallery" align="center" >
  <a href="{{ site.baseurl }}/img/pageimage/grafzno.png">
      <img src="{{ site.baseurl }}/img/pageimage/grafzno.png"/>
  </a>
</div>
<br>
   
**ПРОБНЕ ЗОВНІШНЄ НЕЗАЛЕЖНЕ ОЦІНЮВАННЯ**   
   
<div align="center">
  <a href="https://prob.test.dn.ua/" target="_blank"><img src="/img/pageimage/testportal.png" title="Донецький регіональний центр оцінювання якості освіти" alt="Донецький регіональний центр оцінювання якості освіти"></a>
</div>
   
Запрошуємо випускників взяти участь у пробному ЗНО 2020 року!   
Рекомендуємо переглянути [https://www.youtube.com/watch?v=_u8Wir9Ljek](https://www.youtube.com/watch?v=_u8Wir9Ljek)   
Реєстрація на пробне ЗНО 2020 буде проходити на сайті регіонального центру з 03 по 24 січня 2020 року за посиланням [https://prob.test.dn.ua/](https://prob.test.dn.ua/).   
До 09 та 11 грудня 2019 року відповідно на сайті буде розміщено інформацію про умови реєстрації та вартість послуги із проведення пробного ЗНО.   
Пробне зовнішнє незалежне оцінювання проходить із метою ознайомлення всіх охочих із процедурою проведення зовнішнього незалежного оцінювання та є важливою складовою до його підготовки.