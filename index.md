---
title: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
subtitle: Краматорської міської ради Донецької області<br>Офіційний сайт
layout: page
hero_image: /img/heroimage/1.jpg
hero_height: is-medium
show_sidebar: true
menubar: example_menu
---

<div id="lightgallery" align="center" >
  <a href="{{ site.baseurl }}/img/0001.jpg">
      <img src="{{ site.baseurl }}/img/thumbnail/0001.jpg"/>
  </a>
  <a href="{{ site.baseurl }}/img/0002.jpg">
      <img src="{{ site.baseurl }}/img/thumbnail/0002.jpg"/>
  </a>
</div>
