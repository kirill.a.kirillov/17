---
title: Атестація вчителів
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/7.jpg
show_sidebar: true
menubar: example_menu
---
<nav class="breadcrumb" aria-label="breadcrumbs">
  <ul>
    <li style="margin-top: 0.25em;">
      <a href="/">
        <span class="icon is-small">
          <i class="fas fa-home" aria-hidden="true"></i>
        </span>
        <span>Головна</span>
      </a>
    </li>
    <li class="is-active">
      <a href="/atestatsiya-vchiteliv/">
        <span class="icon is-small">
          <i class="fas fa-chalkboard-teacher" aria-hidden="true"></i> 
        </span>
        <span>Атестація вчителів</span>
      </a>
    </li>
  </ul>
</nav>

<div align="center"><strong>Матеріали з досвіду роботи Сердюк О.О.</strong></div><br>  
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="text-align: center;"><a title="Досвід роботи Сердюк О.О." href="https://drive.google.com/open?id=1mM7Xn3QuMhc1r-54EaHfJ1XdY1xnq8mK" target="_blank" rel="noopener"><i class="far fa-file-powerpoint fa-5x"></i><br>Досвід роботи</a></td>
<td style="text-align: center;"><a title="Досвід роботи Сердюк О.О." href="https://drive.google.com/open?id=1JZqNZWP03R_yskCac45qNDUXbVnlyfcD" target="_blank" rel="noopener"><i class="far fa-file-word fa-5x"></i><br>Досвід роботи</a></td>
</tr>
</tbody>
</table>


<div align="center"><strong>Матеріали з досвіду роботи Бондаревої Г.В.</strong></div><br>   
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="text-align: center;"><a title="Досвід роботи Бондаревої Г.В." href="https://drive.google.com/open?id=1KyPseVnwxYnFNzz9KKJ4be31aG1ckM1n" target="_blank" rel="noopener"><i class="far fa-file-powerpoint fa-5x"></i><br>Досвід роботи</a></td>
<td style="text-align: center;"><a title="Досвід роботи Бондаревої Г.В." href="https://drive.google.com/open?id=115JH4YSxv8_mYi3B7936xv9tL4IZTvo5" target="_blank" rel="noopener"><i class="far fa-file-word fa-5x"></i><br>Досвід роботи</a></td>
</tr>
</tbody>
</table>


<div align="center"><strong>Матеріали з досвіду роботи Карабутки Н.В.</strong></div><br>
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="text-align: center;"><a title="Досвід роботи Карабутки Н.В." href="https://drive.google.com/open?id=1Z0OcaLitZ0p8XxrAlp2fNaYkgSrftQpD" target="_blank" rel="noopener"><i class="far fa-file-powerpoint fa-5x"></i><br>Досвід роботи</a></td>
<td style="text-align: center;"><a title="Досвід роботи Карабутки Н.В." href="https://drive.google.com/file/d/10yS5mjPQ97Tqv6X1THJT-YUHOMRshfm5/view?usp=sharing" target="_blank" rel="noopener"><i class="far fa-file-word fa-5x"></i><br>Досвід роботи</a></td>
</tr>
</tbody>
</table>

 

<div align="center"><strong>Матеріали з досвіду роботи Кондратової О.М.</strong></div><br>
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="text-align: center;"><a title="Досвід роботи Кондратової О.М." href="https://drive.google.com/open?id=14CR1wxAo3VUAQ3NWYvX2Ge2myGcM6wWa" target="_blank" rel="noopener"><i class="far fa-file-powerpoint fa-5x"></i><br>Досвід роботи</a></td>
<td style="text-align: center;"><a title="Досвід роботи Кондратової О.М." href="https://drive.google.com/open?id=1caGoEo-DmO7TdZWbMAxazOR9mrtMLXwA" target="_blank" rel="noopener"><i class="far fa-file-word fa-5x"></i><br>Досвід роботи</a></td>
</tr>
</tbody>
</table>

 

<div align="center"><strong>Матеріали з досвіду роботи Плісс А.Р.</strong></div><br>
<table style="margin-left: auto; margin-right: auto;">
<tbody>
<tr>
<td style="text-align: center;"><a title="Досвід роботи Плісс А.Р." href="https://drive.google.com/open?id=1MLuJS7a-ise66capAoGFlq0ejGEDF1Ag" target="_blank" rel="noopener"><i class="far fa-file-powerpoint fa-5x"></i><br>Досвід роботи</a></td>
<td style="text-align: center;"><a title="Досвід роботи Плісс А.Р." href="https://drive.google.com/open?id=1T4hLUPdernpvbI9yT2BM1EGDUArA34WL" target="_blank" rel="noopener"><i class="far fa-file-word fa-5x"></i><br>Досвід роботи</a></td>
</tr>
</tbody>
</table>

