---
title: Майбутньому першокласнику
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/5.jpg
show_sidebar: true
menubar: example_menu
---
<div align="center"><b>Пам'ятка батькам першокласників</b></div><br>

<div align="right"> 
<i>“Від п'ятилітньої дитини до мене тільки крок, <br>
від народження до  п'ятилітнього - страшна відстань”<br>
Л. Толстой</i><br> 
</div>     

1. Любіть дитину. Не забувайте про тілесний контакт з дитиною. Знаходьте радість у спілкуванні з дітьми. Дайте дитині місце в сім'ї.   
2. Хай не буде жодного дня без прочитаної книжки.   
3. Розмовляйте з дитиною, розвивайте її мову. Цікавтеся справами і проблемами дитини.   
4. Дозволяйте дитині малювати, розфарбовувати, вирізати, наклеювати, ліпити.   
5. Відвідуйте театри, організовуйте сімейні екскурсії містом.   
6. Надайте перевагу повноцінному харчуванню дитини, а не розкішному одягу.   
7. Обмежте перегляд телепередач, ігри на комп'ютері до 30 хвилин.   
8. Привчайте дітей до самообслуговування і формуйте трудові навички і любов до праці.   
9. Не робіть із дитини лише споживача, хай вона буде рівноправним членом сім'ї зі своїми правилами і обов'язками.   
10. Пам'ятайте, що діти — не прості продовжувачі наших особистих умінь і здібностей. Кожна дитина має право на власний вияв своїх потенційних можливостей і на свою власну долю.   
