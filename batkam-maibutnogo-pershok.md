---
title: Батькам майбутнього першокласника
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/4.jpg
show_sidebar: true
menubar: example_menu
---
<div id="lightgallery" align="center" >
  <a href="{{ site.baseurl }}/img/pageimage/do_1_klasu.jpg">
      <img src="{{ site.baseurl }}/img/pageimage/do_1_klasu.jpg"/>
  </a>
</div>
