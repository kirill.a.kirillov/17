---
title: Список учнів
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/7.jpg
show_sidebar: true
menubar: example_menu
---
<div align="center"><strong>Кількості учнів у 2019-2020 н.р.</strong></div>
<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<tbody>
<tr>
<td style="text-align: center;"><strong>Клас</strong></td>
<td style="text-align: center;"><strong>Учнів у класі</strong></td>
<td style="text-align: center;"><strong>Учнів у паралелі</strong></td>
<td style="text-align: center;" colspan="3"><strong>&nbsp;&nbsp;&nbsp;</strong></td>
</tr>
<tr>
<td style="text-align: center;">1а</td>
<td style="text-align: center;">27</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">45</td>
<td style="text-align: center; vertical-align: middle;" rowspan="8">218</td>
<td style="text-align: center; vertical-align: middle;" rowspan="8">218</td>
<td style="text-align: center; vertical-align: middle;" rowspan="21">499</td>
</tr>
<tr>
<td style="text-align: center;">1б</td>
<td style="text-align: center;">18</td>
</tr>
<tr>
<td style="text-align: center;">2а</td>
<td style="text-align: center;">28</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">56</td>
</tr>
<tr>
<td style="text-align: center;">2б</td>
<td style="text-align: center;">28</td>
</tr>
<tr>
<td style="text-align: center;">3а</td>
<td style="text-align: center;">34</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">63</td>
</tr>
<tr>
<td style="text-align: center;">3б</td>
<td style="text-align: center;">29</td>
</tr>
<tr>
<td style="text-align: center;">4а</td>
<td style="text-align: center;">25</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">54</td>
</tr>
<tr>
<td style="text-align: center;">4б</td>
<td style="text-align: center;">29</td>
</tr>
<tr>
<td style="text-align: center;">5а</td>
<td style="text-align: center;">26</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">42</td>
<td style="text-align: center; vertical-align: middle;" rowspan="10">226</td>
<td style="text-align: center; vertical-align: middle;" rowspan="13">281</td>
</tr>
<tr>
<td style="text-align: center;">5б</td>
<td style="text-align: center;">16</td>
</tr>
<tr>
<td style="text-align: center;">6а</td>
<td style="text-align: center;">24</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">50</td>
</tr>
<tr>
<td style="text-align: center;">6б</td>
<td style="text-align: center;">26</td>
</tr>
<tr>
<td style="text-align: center;">7а</td>
<td style="text-align: center;">26</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">48</td>
</tr>
<tr>
<td style="text-align: center;">7б</td>
<td style="text-align: center;">22</td>
</tr>
<tr>
<td style="text-align: center;">8а</td>
<td style="text-align: center;">23</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">47</td>
</tr>
<tr>
<td style="text-align: center;">8б</td>
<td style="text-align: center;">24</td>
</tr>
<tr>
<td style="text-align: center;">9а</td>
<td style="text-align: center;">13</td>
<td style="text-align: center; vertical-align: middle;" rowspan="2">39</td>
</tr>
<tr>
<td style="text-align: center;">9б</td>
<td style="text-align: center;">26</td>
</tr>
<tr>
<td style="text-align: center;">10а</td>
<td style="text-align: center;">25</td>
<td style="text-align: center; vertical-align: middle;" rowspan="3">55</td>
<td style="text-align: center; vertical-align: middle;" rowspan="3">55</td>
</tr>
<tr>
<td style="text-align: center;">11а</td>
<td style="text-align: center;">18</td>
</tr>
<tr>
<td style="text-align: center;">11б</td>
<td style="text-align: center;">12</td>
</tr>
</tbody>
</table>