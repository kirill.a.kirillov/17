---
layout: post
title:  До Дня пам’яті жертв політичних репресій, Дня пам’яті жертв геноциду кримськотатарського народу
subtitle: Новини
date:   2020-05-19 08:00:07
description: Новини
image: /img/postimage/maxresdefault.jpg
published: true
tags: webdev showdev webdesign
canonical_url: https://www.csrhymes.com/2019/07/23/introducing-some-new-layouts-to-bulma-clean-theme.html
---


Про проведення дистанційних тематичних лекцій та уроків до Дня пам’яті жертв політичних репресій, Дня пам’яті жертв геноциду кримськотатарського народу   

Учні старших класів та класні керівники 1-11 класів провели дистанційні тематичні лекції та уроки до Дня пам’яті жертв політичних репресій, Дня пам’яті жертв геноциду кримськотатарського народу використовуючи такі матеріали:

[18 ТРАВНЯ - ДЕНЬ ПАМ'ЯТІ ЖЕРТВ ГЕНОЦИДУ КРИМСЬКОТАТАРСЬКОГО НАРОДУ](https://youtu.be/v-6ZRPCw7eU)   
[Д/ф "Кримські татари: "Крим – наш!" (ф.1)](https://www.youtube.com/watch?v=izD7GAmsYM8)   
[День пам’яті жертв депортації кримських татар / спецефір](https://www.youtube.com/watch?v=WeJ6pDcdguU)   
[Jamala - "1944" | Джамала | Eurovision Winning Performance](https://www.youtube.com/watch?v=cFlZpBN_En4)   
