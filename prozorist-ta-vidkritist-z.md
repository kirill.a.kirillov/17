---
title: Прозорість та відкритість закладу освіти
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/4.jpg
show_sidebar: true
menubar: example_menu
---

* [Статут школи]({{ site.baseurl }}/statut_shkoli)
* [Ліцензія]({{ site.baseurl }}/litsenziya-na-provadzhenn)
* [Структура та органи управління закладу]({{ site.baseurl }}/struktura-ta-organi-uprav)
* [Кадровий склад закладу]({{ site.baseurl }}/kadrovii-sklad)
* [Освітні програми]({{ site.baseurl }}/osvitni-programi-ta-kompo)
* [Територія обслуговування]({{ site.baseurl }}/teritoriya-obslugovuvanny)
* [Фактична кількість осіб, які навчаються у закладі]({{ site.baseurl }}/spisok-uchniv)
* [Мова(мови) освітнього закладу]({{ site.baseurl }}/mova-osvitnogo-protsesu)
* [Наявність вакантних посад]({{ site.baseurl }}/vakansiyi)
* [Матеріально-технічне забезпечення закладу]({{ site.baseurl }}/mat-zabezpechennia)
* [Результати моніторингу якості освіти]({{ site.baseurl }}/rezultati-monitoringu-yak)
* [Річний звіт про діяльність закладу]({{ site.baseurl }}/zvit-pro-dia)
* [Правила прийому до закладу. Порядок зарахування]({{ site.baseurl }}/poryadok-zarakhuvannya-do)
* [Умови доступності для навчання осіб з особливими потребами]({{ site.baseurl }}/umovi-dostupnosti-dlya-na)
* [Кошторис і фінансовий звіт](https://openschool.ua/educational_institutions/301)