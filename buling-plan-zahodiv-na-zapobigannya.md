---
title: План заходів, спрямованих на запобігання та протидію булінгу
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/2.jpg
show_sidebar: true
menubar: example_menu
---
У школі розроблений план заходів, спрямованих на запобігання та протидію булінгу   