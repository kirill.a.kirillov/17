---
title: Структура та органи управління закладу
subtitle: Краматорська загальноосвітня школа І-ІІІ ступенів № 17
layout: page
hero_image: /img/heroimage/9.jpg
show_sidebar: true
menubar: example_menu
---
**СТРУКТУРА ТА ОРГАНИ УПРАВЛІННЯ ЗАКЛАДУ ОСВІТИ**

**Управління закладом освіти**   
1. Система управління закладами освіти визначається законом та установчими документами.
2. Управління закладом освіти в межах повноважень, визначених законами та установчими документами цього закладу, здійснюють:
-засновник (засновники);   
-керівник закладу освіти;   
-колегіальний орган управління закладу освіти – педагогічна рада;   
-колегіальний орган громадського самоврядування;   
-інші органи, передбачені спеціальними законами та/або установчими документами закладу освіти.   

**Права і обов’язки засновника закладу освіти**   
1. Права і обов’язки засновника щодо управління закладом освіти визначаються законами України, установчими документами закладу освіти.
2. Засновник закладу освіти або уповноважена ним особа:   
-затверджує установчі документи закладу освіти, їх нову редакцію та зміни до них;
-затверджує установчі документи закладу освіти, їх нову редакцію та зміни до них;   
-укладає строковий трудовий договір (контракт) з керівником закладу освіти, обраним (призначеним) у порядку, встановленому законодавством та установчими документами закладу освіти;   
-розриває строковий трудовий договір (контракт) з керівником закладу освіти з підстав та у порядку, визначених законодавством та установчими документами закладу освіти;   
-затверджує кошторис та приймає фінансовий звіт закладу освіти у випадках та порядку, визначених законодавством;   
-здійснює контроль за фінансово-господарською діяльністю закладу освіти;   
-здійснює контроль за дотриманням установчих документів закладу освіти;   
-забезпечує створення у закладі освіти інклюзивного освітнього середовища, універсального дизайну та розумного пристосування;   
-здійснює контроль за недопущенням привілеїв чи обмежень (дискримінації) за ознаками раси, кольору шкіри, політичних, релігійних та інших переконань, статі, віку, інвалідності, етнічного та соціального походження, сімейного та майнового стану, місця проживання, за мовними або іншими ознаками;   
-реалізує інші права, передбачені законодавством та установчими документами закладу освіти.   
3. Засновник або уповноважена ним особа не має права втручатися в діяльність закладу освіти, що здійснюється ним у межах його автономних прав, визначених законом та установчими документами.
4. Засновник або уповноважена ним особа може делегувати окремі свої повноваження органу управління закладу освіти та/або наглядовій (піклувальній) раді закладу освіти.
5. Засновник має право створювати заклад освіти, що здійснює освітню діяльність на кількох рівнях освіти.
6. Засновник закладу освіти зобов’язаний:
-забезпечити утримання та розвиток матеріально-технічної бази заснованого ним закладу освіти на рівні, достатньому для виконання вимог стандартів освіти та ліцензійних умов;
-у разі реорганізації чи ліквідації закладу освіти забезпечити здобувачам освіти можливість продовжити навчання на відповідному рівні освіти;
-забезпечити відповідно до законодавства створення в закладі освіти безперешкодного середовища для учасників освітнього процесу, зокрема для осіб з особливими освітніми потребами.

**Керівник закладу освіти**   
   
1. Керівник закладу освіти здійснює безпосереднє управління закладом і несе відповідальність за освітню, фінансово-господарську та іншу діяльність закладу освіти.   
Повноваження (права і обов’язки) та відповідальність керівника закладу освіти визначаються законом та установчими документами закладу освіти.   
Керівник є представником закладу освіти у відносинах з державними органами, органами місцевого самоврядування, юридичними та фізичними особами і діє без довіреності в межах повноважень, передбачених законом та установчими документами закладу освіти.
2. Керівник закладу освіти призначається засновником у порядку, визначеному законами та установчими документами, з числа претендентів, які вільно володіють державною мовою і мають вищу освіту.   
Додаткові кваліфікаційні вимоги до керівника та порядок його обрання (призначення) визначаються спеціальними законами та установчими документами закладу освіти.

**Керівник закладу освіти в межах наданих йому повноважень:**   
   
-організовує діяльність закладу освіти;   
-вирішує питання фінансово-господарської діяльності закладу освіти;   
-призначає на посаду та звільняє з посади працівників, визначає їх функціональні обов’язки;   
-забезпечує організацію освітнього процесу та здійснення контролю за виконанням освітніх програм;   
-забезпечує функціонування внутрішньої системи забезпечення якості освіти;   
-забезпечує умови для здійснення дієвого та відкритого громадського контролю за діяльністю закладу освіти;   
-сприяє та створює умови для діяльності органів самоврядування закладу освіти;   
-сприяє здоровому способу життя здобувачів освіти та працівників закладу освіти;   
-здійснює інші повноваження, передбачені законом та установчими документами закладу освіти.   
-керівництво закладом загальної середньої освіти здійснює директор, повноваження якого визначаються законом, статутом закладу освіти та трудовим договором.   

**Колегіальні органи управління закладів освіти**   

1. Основним колегіальним органом управління закладу освіти є педагогічна рада, яка створюється у випадках і порядку, передбачених спеціальними законами.   
2. Педагогічна рада створюється в усіх закладах освіти, що забезпечують здобуття загальної середньої освіти, незалежно від підпорядкування, типів і форми власності за наявності не менше трьох педагогічних працівників. Усі педагогічні працівники закладу освіти мають брати участь у засіданнях педагогічної ради.   

**Педагогічна рада закладу загальної середньої освіти:**   

-планує роботу закладу;   
-схвалює освітню (освітні) програму (програми) закладу та оцінює результативність її (їх) виконання;   
-формує систему та затверджує процедури внутрішнього забезпечення якості освіти, включаючи систему та механізми забезпечення академічної доброчесності;   
-розглядає питання щодо вдосконалення і методичного забезпечення освітнього процесу;   
-приймає рішення щодо переведення учнів (вихованців) до наступного класу і їх випуску, видачі документів про відповідний рівень освіти, нагородження за успіхи у навчанні;   
-обговорює питання підвищення кваліфікації педагогічних працівників, розвитку їхньої творчої ініціативи, визначає заходи щодо підвищення кваліфікації педагогічних працівників, затверджує щорічний план підвищення кваліфікації педагогічних працівників;   
-розглядає питання впровадження в освітній процес найкращого педагогічного досвіду та інновацій, участі в дослідницькій, експериментальній, інноваційній діяльності, співпраці з іншими закладами освіти, науковими установами, фізичними та юридичними особами, які сприяють розвитку освіти;   
-ухвалює рішення щодо відзначення, морального та матеріального заохочення учнів (вихованців), працівників закладу та інших учасників освітнього процесу;   
-розглядає питання щодо відповідальності учнів (вихованців), працівників закладу та інших учасників освітнього процесу за невиконання ними своїх обов’язків;   
-має право ініціювати проведення позапланового інституційного аудиту закладу та проведення громадської акредитації закладу;   
-розглядає інші питання, віднесені законом та/або статутом закладу до її повноважень.   
-Рішення педагогічної ради закладу загальної середньої освіти вводяться в дію рішеннями керівника закладу.   

**Громадське самоврядування в закладі освіти**   

1. Громадське самоврядування в закладі освіти - це право учасників освітнього процесу як безпосередньо, так і через органи громадського самоврядування колективно вирішувати питання організації та забезпечення освітнього процесу в закладі освіти, захисту їхніх прав та інтересів, організації дозвілля та оздоровлення, брати участь у громадському нагляді (контролі) та в управлінні закладом освіти у межах повноважень, визначених законом та установчими документами закладу освіти.
Громадське самоврядування в закладі освіти здійснюється на принципах, визначених частиною восьмою статті 70 цього Закону.   
У закладі освіти можуть діяти:   
-органи самоврядування працівників закладу освіти;   
-органи самоврядування здобувачів освіти;   
-органи батьківського самоврядування;   
-інші органи громадського самоврядування учасників освітнього процесу.   
2. Вищим колегіальним органом громадського самоврядування закладу освіти є загальні збори (конференція) колективу закладу освіти.   
3. Повноваження, відповідальність, засади формування та діяльності органів громадського самоврядування визначаються спеціальними законами та установчими документами закладу освіти.   
4. У закладах загальної середньої освіти можуть функціонувати методичні об’єднання, що охоплюють учасників освітнього процесу та спеціалістів певного професійного спрямування.   

**Наглядова (піклувальна) рада закладу освіти**   

1. Наглядова (піклувальна) рада закладу освіти створюється за рішенням засновника відповідно до спеціальних законів. Порядок формування наглядової (піклувальної) ради, її відповідальність, перелік і строк повноважень, а також порядок її діяльності визначаються спеціальними законами та установчими документами закладу освіти.   
2. Наглядова (піклувальна) рада закладу освіти сприяє вирішенню перспективних завдань його розвитку, залученню фінансових ресурсів для забезпечення його діяльності з основних напрямів розвитку і здійсненню контролю за їх використанням, ефективній взаємодії закладу освіти з органами державної влади та органами місцевого самоврядування, науковою громадськістю, громадськими організаціями, юридичними та фізичними особами.   
3. Члени наглядової (піклувальної) ради закладу освіти мають право брати участь у роботі колегіальних органів закладу освіти з правом дорадчого голосу.   
4. До складу наглядової (піклувальної) ради закладу освіти не можуть входити здобувачі освіти та працівники цього закладу освіти.   

**Піклувальна рада має право:**   

-брати участь у визначенні стратегії розвитку закладу освіти та контролювати її виконання;   
-сприяти залученню додаткових джерел фінансування;   
-аналізувати та оцінювати діяльність закладу освіти та його керівника;   
-контролювати виконання кошторису та/або бюджету закладу освіти і вносити відповідні рекомендації та пропозиції, що є обов’язковими для розгляду керівником закладу освіти;   
-вносити засновнику закладу освіти подання про заохочення або відкликання керівника закладу освіти з підстав, визначених законом;   
-здійснювати інші права, визначені спеціальними законами та/або установчими документами закладу освіти.   

Особливості відносин між закладами освіти та політичними партіями (об’єднаннями) і релігійними організаціями   

1. Державні та комунальні заклади освіти відокремлені від церкви (релігійних організацій), мають світський характер.   
2. Приватні заклади освіти, зокрема засновані релігійними організаціями, мають право визначати релігійну спрямованість власної освітньої діяльності.   
3. Політичні партії (об’єднання) не мають права втручатися в освітню діяльність закладів освіти.   
У закладах освіти забороняється створення осередків політичних партій та функціонування будь-яких політичних об’єднань.      
4. Керівництву закладів освіти, педагогічним, науково-педагогічним і науковим працівникам, органам державної влади та органам місцевого самоврядування, їх посадовим особам забороняється залучати здобувачів освіти до участі в заходах, організованих релігійними організаціями (крім закладів освіти, визначених частиною другою цієї статті) чи політичними партіями (об’єднаннями), крім заходів, передбачених освітньою програмою.   
5. Керівництву закладів освіти, органам державної влади та органам місцевого самоврядування, їх посадовим особам забороняється залучати працівників закладів освіти до участі в заходах, організованих релігійними організаціями (крім закладів освіти, визначених частиною другою цієї статті) чи політичними партіями (об’єднаннями).   